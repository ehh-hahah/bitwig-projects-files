[INFO]

Gelato Game 

110BPM

C major

All MIDI tracks are recorded with rich MPE information - for each note in each track there is a pitch modulation, pressure, timbre and velocity automation/information saved. 

Track ends at 3:55 and onwards there is random improvisation on all instruments at once lol, you can just ignore that or reuse the MIDI. 

‘Plucky ARP capture’ is the same as ‘Plucky ARP’, but with MIDI notes from arpeggiator being frozen. 

The track was inspired by some gelato ice-cream shop on my street, that seems to not have too many clients and often I can see the owner, who is taking care of her two children during the work. Idk, that picture made me kinda romantic xd And that’s what I was thinking about when creating the song, nothing more.